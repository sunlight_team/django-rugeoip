import os

SECRET_KEY = 'fake-key'

INSTALLED_APPS = [
    "geo",
    "tests",
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    },
}

try:
    GEOIP_PATH = os.environ['GEOIP_PATH']
except KeyError:
    raise Exception('env variable GEOIP_PATH expected')
