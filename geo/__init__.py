# -*- coding: utf-8 -*-
from django.contrib.gis.geoip import GeoIP as DjangoGeoIP, GeoIPException
from geo.ipgeobase import GeoIP as IPGeoBase


class GeoIP(object):
    def __init__(self, path=None, cache=0, country=None, city=None):
        self._geoip = DjangoGeoIP(path=path, cache=cache)
        self._ipgeobase = IPGeoBase(path=path, cache=cache)

    def city(self, query):
        rec = self._ipgeobase.city(query) or {}
        if rec.get('country_code') in ('RU', 'UA'):
            if rec['country_code'] == 'RU':
                rec['country_name'] = u'Russian Federation'
            elif rec['country_code'] == 'UA':
                rec['country_name'] = u'Ukraine'
        else:
            return self._geoip.city(query)

        return rec

    country = city

    def country_code(self, query):
        rec = self.city(query) or {}
        return rec.get('country_code', '')
